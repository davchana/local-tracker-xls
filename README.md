An HTML5 offline-online webapp to store daily WUT, X, Steps.

Link: [https://davchana.gitlab.io/local-tracker-xls](https://davchana.gitlab.io/local-tracker-xls)

#### Why:

I track my daily steps, WUT, X from mid-2014. Earlier I used to print a monthly sheet

& paste it on wall, & update it on daily basis. On month end all this data goes in a spreadsheet,

to show trends in charts. Somewhere in between I started using yellow post-it notes to record the

data in morning, & then copy it to spreadsheet when I get time. Overtime I started loosing/misplacing

these post-it notes, thus creating gaps in my spreadsheet. This app, once browsed to on any device, 

will work offline, & will store these three types of data.

#### Specs:

  1. Should use local-storage at device.
  2. Should be availbe off-line, so should use manifest.
  3. Should not ineterfere with data stored by other apps from same domain.
 